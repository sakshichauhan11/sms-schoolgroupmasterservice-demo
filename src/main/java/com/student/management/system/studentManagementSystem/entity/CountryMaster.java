package com.student.management.system.studentManagementSystem.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the COUNTRY_MASTER database table.
 * 
 */
@Entity
@Table(name="COUNTRY_MASTER")
@NamedQuery(name="CountryMaster.findAll", query="SELECT c FROM CountryMaster c")
public class CountryMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="COUNTRY_ID", unique=true, nullable=false)
	private long countryId;

	@Column(name="COUNTRY_CODE", length=5)
	private String countryCode;

	@Column(name="COUNTRY_NAME", length=50)
	private String countryName;

	//bi-directional many-to-one association to StateMaster
	@OneToMany(mappedBy="countryMaster")
	private List<StateMaster> stateMasters;

	public CountryMaster() {
	}

	public long getCountryId() {
		return this.countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public List<StateMaster> getStateMasters() {
		return this.stateMasters;
	}

	public void setStateMasters(List<StateMaster> stateMasters) {
		this.stateMasters = stateMasters;
	}

	public StateMaster addStateMaster(StateMaster stateMaster) {
		getStateMasters().add(stateMaster);
		stateMaster.setCountryMaster(this);

		return stateMaster;
	}

	public StateMaster removeStateMaster(StateMaster stateMaster) {
		getStateMasters().remove(stateMaster);
		stateMaster.setCountryMaster(null);

		return stateMaster;
	}

}