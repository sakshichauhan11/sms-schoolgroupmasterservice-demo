package com.student.management.system.studentManagementSystem.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the CLASS_MASTER database table.
 * 
 */
@Entity
@Table(name="CLASS_MASTER")
@NamedQuery(name="ClassMaster.findAll", query="SELECT c FROM ClassMaster c")
public class ClassMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CLASS_ID", unique=true, nullable=false)
	private long classId;

	@Column(name="\"SECTION\"", length=20)
	private String section;

	@Column(length=20)
	private String standard;

	//bi-directional many-to-one association to MarksRecord
	@OneToMany(mappedBy="classMaster")
	private List<MarksRecord> marksRecords;

	public ClassMaster() {
	}

	public long getClassId() {
		return this.classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public List<MarksRecord> getMarksRecords() {
		return this.marksRecords;
	}

	public void setMarksRecords(List<MarksRecord> marksRecords) {
		this.marksRecords = marksRecords;
	}

	public MarksRecord addMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().add(marksRecord);
		marksRecord.setClassMaster(this);

		return marksRecord;
	}

	public MarksRecord removeMarksRecord(MarksRecord marksRecord) {
		getMarksRecords().remove(marksRecord);
		marksRecord.setClassMaster(null);

		return marksRecord;
	}

}