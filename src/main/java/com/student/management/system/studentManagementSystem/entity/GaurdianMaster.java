package com.student.management.system.studentManagementSystem.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the GAURDIAN_MASTER database table.
 * 
 */
@Entity
@Table(name="GAURDIAN_MASTER")
@NamedQuery(name="GaurdianMaster.findAll", query="SELECT g FROM GaurdianMaster g")
public class GaurdianMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="GAURDIAN_ID", unique=true, nullable=false)
	private long gaurdianId;

	//bi-directional many-to-one association to PersonalInfo
	@ManyToOne
	@JoinColumn(name="PERSON_ID")
	private PersonalInfo personalInfo;

	//bi-directional many-to-one association to StudentMaster
	@OneToMany(mappedBy="gaurdianMaster")
	private List<StudentMaster> studentMasters;

	public GaurdianMaster() {
	}

	public long getGaurdianId() {
		return this.gaurdianId;
	}

	public void setGaurdianId(long gaurdianId) {
		this.gaurdianId = gaurdianId;
	}

	public PersonalInfo getPersonalInfo() {
		return this.personalInfo;
	}

	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}

	public List<StudentMaster> getStudentMasters() {
		return this.studentMasters;
	}

	public void setStudentMasters(List<StudentMaster> studentMasters) {
		this.studentMasters = studentMasters;
	}

	public StudentMaster addStudentMaster(StudentMaster studentMaster) {
		getStudentMasters().add(studentMaster);
		studentMaster.setGaurdianMaster(this);

		return studentMaster;
	}

	public StudentMaster removeStudentMaster(StudentMaster studentMaster) {
		getStudentMasters().remove(studentMaster);
		studentMaster.setGaurdianMaster(null);

		return studentMaster;
	}

}