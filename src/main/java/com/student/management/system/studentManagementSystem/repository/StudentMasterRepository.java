package com.student.management.system.studentManagementSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.management.system.studentManagementSystem.entity.StudentMaster;

@Repository
public interface StudentMasterRepository extends JpaRepository<StudentMaster, Long>{

}
