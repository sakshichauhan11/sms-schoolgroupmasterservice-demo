package com.student.management.system.studentManagementSystem.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the EXAM_TYPE database table.
 * 
 */
@Entity
@Table(name="EXAM_TYPE")
@NamedQuery(name="ExamType.findAll", query="SELECT e FROM ExamType e")
public class ExamType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="EXAM_TYPE_ID", unique=true, nullable=false)
	private long examTypeId;

	@Column(name="EXAM_TYPE_NAME", nullable=false, length=50)
	private String examTypeName;

	//bi-directional many-to-one association to ExamMaster
	@OneToMany(mappedBy="examType")
	private List<ExamMaster> examMasters;

	public ExamType() {
	}

	public long getExamTypeId() {
		return this.examTypeId;
	}

	public void setExamTypeId(long examTypeId) {
		this.examTypeId = examTypeId;
	}

	public String getExamTypeName() {
		return this.examTypeName;
	}

	public void setExamTypeName(String examTypeName) {
		this.examTypeName = examTypeName;
	}

	public List<ExamMaster> getExamMasters() {
		return this.examMasters;
	}

	public void setExamMasters(List<ExamMaster> examMasters) {
		this.examMasters = examMasters;
	}

	public ExamMaster addExamMaster(ExamMaster examMaster) {
		getExamMasters().add(examMaster);
		examMaster.setExamType(this);

		return examMaster;
	}

	public ExamMaster removeExamMaster(ExamMaster examMaster) {
		getExamMasters().remove(examMaster);
		examMaster.setExamType(null);

		return examMaster;
	}

}