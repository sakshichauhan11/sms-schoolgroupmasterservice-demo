package com.student.management.system.studentManagementSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.student.management.system.studentManagementSystem.entity.SchoolGroupMaster;

@Repository
public interface SchoolGroupMasterRepository extends JpaRepository<SchoolGroupMaster, Long>{

}
