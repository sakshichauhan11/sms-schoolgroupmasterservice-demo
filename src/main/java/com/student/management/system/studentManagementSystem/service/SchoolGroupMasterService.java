package com.student.management.system.studentManagementSystem.service;

import java.util.List;

import com.student.management.system.studentManagementSystem.entity.SchoolGroupMaster;

public interface SchoolGroupMasterService {

	List<SchoolGroupMaster> findAll();

	SchoolGroupMaster addSchoolGroup(SchoolGroupMaster group);

}
