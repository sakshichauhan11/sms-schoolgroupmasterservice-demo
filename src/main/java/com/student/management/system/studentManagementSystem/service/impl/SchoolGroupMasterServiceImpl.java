package com.student.management.system.studentManagementSystem.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.management.system.studentManagementSystem.entity.SchoolGroupMaster;
import com.student.management.system.studentManagementSystem.repository.SchoolGroupMasterRepository;
import com.student.management.system.studentManagementSystem.service.SchoolGroupMasterService;

@Service
public class SchoolGroupMasterServiceImpl implements SchoolGroupMasterService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	SchoolGroupMasterRepository schoolGroupMasterRepository;

	@Override
	public List<SchoolGroupMaster> findAll() {
		logger.info("SchoolGroup Master Service find all................................");
		return schoolGroupMasterRepository.findAll();
	}
	
	@Override
	 public SchoolGroupMaster addSchoolGroup(SchoolGroupMaster group) {
		logger.info("SchoolGroup Master Service add group................................");
         return this.schoolGroupMasterRepository.save(group);
     }

}
