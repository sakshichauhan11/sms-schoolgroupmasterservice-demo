package com.student.management.system.studentManagementSystem.service;

import java.util.List;

import com.student.management.system.studentManagementSystem.entity.StudentMaster;

public interface StudentMasterService {

	List<StudentMaster> findAll();

	StudentMaster addStudent(StudentMaster student);

}
