package com.student.management.system.studentManagementSystem.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.student.management.system.studentManagementSystem.entity.SchoolGroupMaster;
import com.student.management.system.studentManagementSystem.service.SchoolGroupMasterService;

@RestController
public class SchoolGroupMasterController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SchoolGroupMasterService schoolGroupMasterService;

	@GetMapping("school-group/all")
	public List<SchoolGroupMaster> getAllSchoolGroup() {
		List<SchoolGroupMaster> group = schoolGroupMasterService.findAll();
		logger.info("SchoolGroup Master Controller get all................................");
		return group;
	}
	
	@PostMapping("school-group/add")
	public SchoolGroupMaster createSchoolGroup(@RequestBody SchoolGroupMaster group) {
		SchoolGroupMaster addedGroup = schoolGroupMasterService.addSchoolGroup(group);
		logger.info("SchoolGroup Master Controller add................................");
		return addedGroup;
	}
}
