package com.student.management.system.studentManagementSystem.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the CITY_MASTER database table.
 * 
 */
@Entity
@Table(name="CITY_MASTER")
@NamedQuery(name="CityMaster.findAll", query="SELECT c FROM CityMaster c")
public class CityMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CITY_ID", unique=true, nullable=false)
	private long cityId;

	@Column(name="CITY_NAME", length=100)
	private String cityName;

	@Column(name="DISTRICT_ID")
	private BigDecimal districtId;

	//bi-directional many-to-one association to StateMaster
	@ManyToOne
	@JoinColumn(name="STATE_ID")
	private StateMaster stateMaster;

	//bi-directional many-to-one association to PincodeMaster
	@OneToMany(mappedBy="cityMaster")
	private List<PincodeMaster> pincodeMasters;

	public CityMaster() {
	}

	public long getCityId() {
		return this.cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public BigDecimal getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(BigDecimal districtId) {
		this.districtId = districtId;
	}

	public StateMaster getStateMaster() {
		return this.stateMaster;
	}

	public void setStateMaster(StateMaster stateMaster) {
		this.stateMaster = stateMaster;
	}

	public List<PincodeMaster> getPincodeMasters() {
		return this.pincodeMasters;
	}

	public void setPincodeMasters(List<PincodeMaster> pincodeMasters) {
		this.pincodeMasters = pincodeMasters;
	}

	public PincodeMaster addPincodeMaster(PincodeMaster pincodeMaster) {
		getPincodeMasters().add(pincodeMaster);
		pincodeMaster.setCityMaster(this);

		return pincodeMaster;
	}

	public PincodeMaster removePincodeMaster(PincodeMaster pincodeMaster) {
		getPincodeMasters().remove(pincodeMaster);
		pincodeMaster.setCityMaster(null);

		return pincodeMaster;
	}

}