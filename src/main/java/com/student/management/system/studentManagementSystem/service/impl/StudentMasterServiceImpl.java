package com.student.management.system.studentManagementSystem.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.student.management.system.studentManagementSystem.entity.StudentMaster;
import com.student.management.system.studentManagementSystem.repository.StudentMasterRepository;
import com.student.management.system.studentManagementSystem.service.StudentMasterService;

@Service
public class StudentMasterServiceImpl implements StudentMasterService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	StudentMasterRepository studentMasterRepository;

	@Override
	public List<StudentMaster> findAll() {
		logger.info("Student Master Service find all................................");
		return studentMasterRepository.findAll();
	}

	@Override
	public StudentMaster addStudent(StudentMaster student) {
		logger.info("Student Master Service add student................................");
		return this.studentMasterRepository.save(student);
	}
}
