package com.student.management.system.studentManagementSystem.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.student.management.system.studentManagementSystem.entity.StudentMaster;
import com.student.management.system.studentManagementSystem.service.StudentMasterService;

@RestController
public class StudentMasterController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private StudentMasterService studentMasterService;

	@GetMapping("student-master/all")
	public List<StudentMaster> getAllStudentsRecord() {
		logger.info("Student Master Controller get all Enter");
		List<StudentMaster> student = studentMasterService.findAll();
		logger.info("Student Master Controller get all Exit Successfully");
		return student;
	}
	
	@PostMapping("student-master/add")
	public StudentMaster createStudent(@RequestBody StudentMaster student) {
		StudentMaster studentMaster = studentMasterService.addStudent(student);
		logger.info("Student Master Controller add................................");
		return studentMaster;
	}
}
