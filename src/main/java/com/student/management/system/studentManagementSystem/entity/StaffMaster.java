package com.student.management.system.studentManagementSystem.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the STAFF_MASTER database table.
 * 
 */
@Entity
@Table(name="STAFF_MASTER")
@NamedQuery(name="StaffMaster.findAll", query="SELECT s FROM StaffMaster s")
public class StaffMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STAFF_ID", unique=true, nullable=false)
	private long staffId;

	@Column(length=20)
	private String specialization;

	//bi-directional many-to-one association to PersonalInfo
	@ManyToOne
	@JoinColumn(name="PERSON_ID")
	private PersonalInfo personalInfo;

	public StaffMaster() {
	}

	public long getStaffId() {
		return this.staffId;
	}

	public void setStaffId(long staffId) {
		this.staffId = staffId;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public PersonalInfo getPersonalInfo() {
		return this.personalInfo;
	}

	public void setPersonalInfo(PersonalInfo personalInfo) {
		this.personalInfo = personalInfo;
	}

}